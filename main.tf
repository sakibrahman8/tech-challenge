terraform {
  required_version = ">= 0.12"

  //backend "s3" {
  //  bucket         = "terraform-state-bucket"
  //  key            = "terraform.tfstate"
  //  region         = var.region
  //  dynamodb_table = "terraform-lock"
  //  encrypt        = true
  //}
  /*
  Commenting this out as I don't have access to S3 buckets nor DynamoDB.
  Generally it's best practice to manage state remotely
  I prefer managing state within the cloud provider, in this case AWS for the following reasons:
    - managed service
    - highly available & durable
    - supports versioning
    - supports locking (through DynamoDB), which will eliminate issues caused by multiple team members running TF at the same time.
  */

}


provider "aws" {
  region = var.region
}

module "vpc" {
  source            = "./vpc"
  vpc_cidr_block    = var.vpc_cidr_block
  subnet_cidr_block = var.subnet_cidr_block
  route_cidr_block  = var.route_cidr_block
}

module "security_group" {
  source              = "./security_group"
  vpc_id              = module.vpc.vpc_id
  security_group_name = var.security_group_name
  ingress_rules       = var.ingress_rules
  egress_rules        = var.egress_rules
}

module "ec2_instance" {
  source                   = "./ec2_instance"
  ami_id                   = var.ami_id
  instance_type            = var.instance_type
  key_name                 = var.key_name
  subnet_id                = module.vpc.subnet_id
  security_group_id        = module.security_group.sg_id
  root_volume_size         = var.root_volume_size
  extra_volume_size        = var.extra_volume_size
  extra_volume_mount_point = var.extra_volume_mount_point
  extra_volume_device_name = var.extra_volume_device_name
}
