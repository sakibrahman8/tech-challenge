# Terraform AWS Infrastructure Project

This repository contains the Terraform modules for setting up AWS infrastructure, including VPC, Security Group, and EC2 instances with additional EBS volumes.

## Why I Used Modules?

Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. They are used for creating reusable components, improving organization, and treating pieces of infrastructure as a single entity.

Reasons we use modules:
- **Reusability**: Write once, use multiple times. You can reuse modules across multiple environments ensuring there is no repitition in code.
- **Organization**: Modules help in segregating different resources logically, making the code easier to understand and manage.
- **Abstraction**: Users of the modules do not have to worry about the underlying implementations. They can utilize the module functionalities without knowing the detailed configurations.

## Why Variable Driven?

The use of variables makes it possible to run the same code but with different settings, allowing us to use the same base configuration in different environments, and to share configurations between different users without exposing sensitive information.

Reasons we use a variable-driven approach:
- **Flexibility**: Changes can be made easily by simply altering the variable values.
- **Security**: Sensitive data such as passwords, private keys, etc., are passed as variables, preventing them from being hardcoded into the configurations.
- **Scalability**: It’s easier to scale resources up or down by changing variable values.

## How to Run

1. **Initialize the Terraform working directory**:

    ```bash
    terraform init
    ```

2. **Create a new plan and save it to the `tfplan` file**:

    ```bash
    terraform plan -var-file=VALUES.tfvars -out=tfplan
    ```

3. **Apply the plan stored in the `tfplan` file**:

    ```bash
    terraform apply "tfplan"
    ```

## Requirements

- You should have Terraform v0.12 (or later) installed on your local system.
- You should have an AWS account with sufficient permissions to create and manage the required resources.
- You should configure your AWS credentials on your local system, either using environment variables, AWS CLI, or the shared credentials file.
- You need to replace the variables in `terraform.tfvars` with your actual values.
