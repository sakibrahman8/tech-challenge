region                   = "eu-west-1"
ami_id                   = "ami-0c1f3a8058fde8814" //The doc provided, "ami-002070d43b0a4f171", but this doesn't exist
instance_type            = "t2.micro"
key_name                 = "iv2_5"
vpc_cidr_block           = "10.0.0.0/16"
subnet_cidr_block        = "10.0.1.0/24"
route_cidr_block         = "0.0.0.0/0"
root_volume_size         = 10
extra_volume_size        = 10
extra_volume_mount_point = "/opt/iv2"
extra_volume_device_name = "/dev/sdf"
security_group_name      = "allow_ssh_and_http"
ingress_rules = [
  {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  },
  {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
]
egress_rules = [
  {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
]
