variable "region" {
  description = "AWS region"
  default     = "eu-west-1"
}

variable "ami_id" {
  description = "AMI ID for the EC2 instance"
  default     = "ami-0c1f3a8058fde8814" //The doc provided, "ami-002070d43b0a4f171", but this doesn't exist
}

variable "instance_type" {
  description = "Instance type for the EC2 instance"
  default     = "t2.micro"
}

variable "key_name" {
  description = "SSH key pair name for the EC2 instance"
}

variable "vpc_cidr_block" {
  description = "CIDR block for the VPC"
  default     = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  description = "CIDR block for the subnet"
  default     = "10.0.1.0/24"
}

variable "route_cidr_block" {
  description = "CIDR block for the route"
  default     = "0.0.0.0/0"
}

variable "root_volume_size" {
  description = "Size of the root volume of the EC2 instance"
  default     = 10
}

variable "extra_volume_size" {
  description = "Size of the extra EBS volume to attach to the EC2 instance"
  default     = 10
}

variable "extra_volume_mount_point" {
  description = "Mount point for the extra EBS volume (used for tagging)"
  default     = "/opt/iv2"
}

variable "extra_volume_device_name" {
  description = "Device name for the extra EBS volume"
  default     = "/dev/sdf"
}

variable "security_group_name" {
  description = "Name of the security group"
  default     = "allow_ssh_and_http"
}

variable "ingress_rules" {
  description = "List of ingress rules"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = []
}

variable "egress_rules" {
  description = "List of egress rules"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = []
}
