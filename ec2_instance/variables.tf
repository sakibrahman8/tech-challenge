variable "ami_id" {}
variable "instance_type" {}
variable "key_name" {}
variable "subnet_id" {}
variable "security_group_id" {}

variable "root_volume_size" {}
variable "extra_volume_size" {}
variable "extra_volume_mount_point" {}
variable "extra_volume_device_name" {}

