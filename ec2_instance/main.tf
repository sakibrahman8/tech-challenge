resource "aws_instance" "web" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  subnet_id              = var.subnet_id
  vpc_security_group_ids = [var.security_group_id]

  root_block_device {
    volume_size           = var.root_volume_size
    volume_type           = "gp2"
    delete_on_termination = true
  }
}

resource "aws_ebs_volume" "extra" {
  availability_zone = aws_instance.web.availability_zone
  size              = var.extra_volume_size
  type              = "gp2"
  tags = {
    Mount = var.extra_volume_mount_point
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = var.extra_volume_device_name
  volume_id   = aws_ebs_volume.extra.id
  instance_id = aws_instance.web.id
}
